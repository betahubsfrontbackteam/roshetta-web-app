import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestominalsComponent } from './testominals.component';

describe('TestominalsComponent', () => {
  let component: TestominalsComponent;
  let fixture: ComponentFixture<TestominalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestominalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestominalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
