import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HowToPharmaciesComponent } from './how-to-pharmacies.component';

describe('HowToPharmaciesComponent', () => {
  let component: HowToPharmaciesComponent;
  let fixture: ComponentFixture<HowToPharmaciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowToPharmaciesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HowToPharmaciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
