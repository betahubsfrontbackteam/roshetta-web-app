import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {HttpClientModule, HttpClient} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';


import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { HeaderComponent } from './header/header.component';
import { SignInPageComponent } from './sign-in-page/sign-in-page.component';
import { SignUpPageComponent } from './sign-up-page/sign-up-page.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { SideSliderComponent } from './shared/side-slider/side-slider.component';
import { HeaderLandingComponent } from './header-landing/header-landing.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ArticleComponent } from './home-page/article/article.component';
import { HowToComponent } from './home-page/how-to/how-to.component';
import { HowToPharmaciesComponent } from './home-page/how-to-pharmacies/how-to-pharmacies.component';
import { TestominalsComponent } from './home-page/testominals/testominals.component';
import { PartnersComponent } from './home-page/partners/partners.component';
import { ContactUsComponent } from './home-page/contact-us/contact-us.component';


export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HeaderComponent,
    SignInPageComponent,
    SignUpPageComponent,
    ForgetPasswordComponent,
    SideSliderComponent,
    HeaderLandingComponent,
    HomePageComponent,
    ArticleComponent,
    HowToComponent,
    HowToPharmaciesComponent,
    TestominalsComponent,
    PartnersComponent,
    ContactUsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
